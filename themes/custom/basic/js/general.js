
(function ($) {
  $(document).ready(function () {

    // General
    $("#btn-menu").click(function () {
      $("#main-menu").toggleClass("active");
    });

    // Home
    $("#read").click(function () {
      $(".more-read").toggleClass("active");
      $(".more-shared").toggleClass("active");
    });

    $("#shared").click(function () {
      $(".more-shared").toggleClass("active");
      $(".more-read").toggleClass("active");
    });

      // last news
    $(".last_news .views-element-container >div").owlCarousel({
      margin: 0,
      nav: true,
      loop: false,
      navRewind: false,
      paddding: 0,
      center: true,
      responsive: {
        479: {
          items: 1
        },
        1024: {
          items: 2
        },
        1180: {
          items: 4
        }
      }
    });

    $(".view_news.more-read .views-element-container >div").owlCarousel({
      margin: 0,
      nav: true,
      loop: false,
      navRewind: false,
      paddding: 0,
      center: true,
      responsive: {
        479: {
          items: 1
        },
        1024: {
          items: 2
        },
        1180: {
          items: 4
        }
      }
    });

    $(".view_news.more-shared .views-element-container >div").owlCarousel({
      margin: 0,
      nav: true,
      loop: false,
      navRewind: false,
      paddding: 0,
      center: true,
      responsive: {
        479: {
          items: 1
        },
        1024: {
          items: 2
        },
        1180: {
          items: 4
        }
      }
    });


  });
})(jQuery);
